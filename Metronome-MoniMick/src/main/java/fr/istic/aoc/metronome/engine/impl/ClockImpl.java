package fr.istic.aoc.metronome.engine.impl;

import fr.istic.aoc.metronome.command.ICommand;
import fr.istic.aoc.metronome.command.impl.TickCommand;
import fr.istic.aoc.metronome.engine.IClock;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Defines the Clock firing the Tick event.
 * @@author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 * @see fr.istic.aoc.metronome.engine.IClock
 */
public class ClockImpl implements IClock {

    /* Attributes */
    TickCommand tickCommand;
    ScheduledExecutorService scheduledPool;

    /* Constructor */
    public ClockImpl() {
        scheduledPool = null;
    }


    @Override
    public void setTickCommand(TickCommand tick, double bpm) {
        scheduledPool = Executors.newScheduledThreadPool(1);
        tickCommand = tick;
        activatePeriodically(bpm);
    }

    @Override
    public void activatePeriodically(double bpm) {
        try {
            float period_float = (float)60/(float)bpm ;
            int period =(int) (period_float * 1000) ;
            scheduledPool.scheduleAtFixedRate(()->tickCommand.execute(), 0, period, TimeUnit.MILLISECONDS);
        }  catch(Exception e){
            System.out.print("Error <Clock>.activatePeriodically(int bpm) "+e.getMessage());
            System.err.print("Error <Clock>.activatePeriodically(int bpm) "+e.getMessage());
        }
    }

    @Override
    public void desactivateTickCommand() {
        try {
            //scheduledPool.shutdown();
            scheduledPool.shutdownNow();
        }   catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public TickCommand getTickCommand() {
        return tickCommand;
    }
}
