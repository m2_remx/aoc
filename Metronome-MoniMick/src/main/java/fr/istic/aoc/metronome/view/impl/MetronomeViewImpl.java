package fr.istic.aoc.metronome.view.impl;

import fr.istic.aoc.metronome.command.ICommand;
import fr.istic.aoc.metronome.command.impl.*;
import fr.istic.aoc.metronome.controller.IController;
import fr.istic.aoc.metronome.controller.impl.ControllerImpl;
import fr.istic.aoc.metronome.view.IMetronomeView;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.media.AudioClip;
import javafx.scene.text.Font;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by leiko on 23/10/15.
 * Modified by Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 */
public class MetronomeViewImpl implements IMetronomeView {

    /* Attributes */
    private IController controller;

    /* Images */
    private Image ledOn;
    private Image ledOff;
    /* Sound */
    private AudioClip kick;
    private AudioClip snare;
    private AudioClip beep;

    /* Commands */
    private ICommand startCmd;
    private ICommand stopCmd;
    private ICommand incCmd;
    private ICommand decCmd;
    private ICommand bpmCmd;

    /* FX Components */
    @FXML
    BorderPane body;

    @FXML
    TextField display;

    @FXML
    TextField measureView;

    @FXML
    ImageView led1;

    @FXML
    ImageView led2;

    @FXML
    Slider sliderBPM;

    @FXML
    Button buttonINC;

    @FXML
    Button buttonDEC;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        /* Initialize Slider Event */
        initSliderBPMEvent();
        /* Initialize LED Images */
        ledOn = new Image(getClass().getResource("/style/on.png").toExternalForm());
        ledOff = new Image(getClass().getResource("/style/off.png").toExternalForm());
        /* Initialize BIP Sounds */
        kick = new AudioClip(getClass().getResource("/kick.wav").toExternalForm());
        kick.play(100);
        //snare = new AudioClip(getClass().getResource("/snare.wav").toExternalForm());
        beep = new AudioClip(getClass().getResource("/beep.mp3").toExternalForm());
        /* Disable some controls of the view */
        disableControls(true);
    }

    /**
     * Enable (Disable) some controls of the view whether the engine is on (or not) .
     * @param bool
     */
    private void disableControls(boolean bool) {
        sliderBPM.setDisable(bool);
        buttonDEC.setDisable(bool);
        buttonINC.setDisable(bool);
    }

    /**
     *
     * @param event
     */
    @FXML
    public void startEvent(ActionEvent event) {
        startCmd.execute();
        disableControls(false);
    }

    /**
     *
     * @param event
     */
    @FXML
    public void stopEvent(ActionEvent event) {
        stopCmd.execute();
        disableControls(true);
    }

    /**
     *
     * @param event
     */
    @FXML
    public void incEvent(ActionEvent event) {
        incCmd.execute();
    }

    @FXML
    public void decEvent(ActionEvent event) {
        decCmd.execute();
    }

    /**
     * Event Listener for the BPM Slider
     */
    public void initSliderBPMEvent() {
        sliderBPM.valueProperty().addListener((observable, oldValue, newValue) -> {
            bpmCmd.execute();
        });
    }

    /**
     *
     * Try to install a new Font for the BPM Screen
     */
    @Deprecated
    private void setFontForDisplayView() {
        Font.loadFont(getClass().getResource("/digitalfont/DS-DIGIB.TTF").toExternalForm(), 10);
    }


    public void turnOnLED2() {
        /* Run into JavaFX Thread */
        Platform.runLater(() -> {
            led2.setImage(ledOn);
            kick.play();
            Timer timer = new Timer();
            timer.schedule(
                    new TimerTask() {
                        @Override
                        public void run() {
                            Platform.runLater(() -> {
                                led2.setImage(ledOff);
                            });
                        }
                    }, 120
            );
        });
    }


    public void turnOnLED1() {
        /* Run into JavaFX Thread */
        Platform.runLater(() -> {
            led1.setImage(ledOn);
            beep.play();
            Timer timer = new Timer();
            timer.schedule(
                    new TimerTask() {
                        @Override
                        public void run() {
                            Platform.runLater(() -> {
                                led1.setImage(ledOff);
                            });
                        }
                    }, 120
            );
        });
    }

    /**
     *
     * @return the value of the slider of the Metronome
     */
    @Override
    public double getBPMValue() {
        return new Double(this.sliderBPM.getValue()).intValue();
    }

    @Override
    public void printMeasure(int measure) {
        this.measureView.setText(measure + "");
    }

    @Override
    public void printBPM(double bpm) {
        this.display.setText(new Double(bpm).intValue()+"");
    }


    //*   -      Commands      -   *//

    @Override
    public void setStartCmd(ICommand startCmd) {
        this.startCmd = startCmd;
    }

    @Override
    public void setStopCmd(ICommand stopCmd) {
        this.stopCmd = stopCmd;
    }

    @Override
    public void setIncCmd(ICommand incCmd) {
        this.incCmd = incCmd;
    }

    @Override
    public void setDecCmd(ICommand decCmd) {
        this.decCmd = decCmd;
    }

    @Override
    public void setBPMCmd(ICommand bpmCommand) {
        this.bpmCmd = bpmCommand;
    }

}