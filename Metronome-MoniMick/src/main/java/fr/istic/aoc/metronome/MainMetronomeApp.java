package fr.istic.aoc.metronome;

import fr.istic.aoc.metronome.controller.IController;
import fr.istic.aoc.metronome.controller.impl.ControllerImpl;
import fr.istic.aoc.metronome.engine.IClock;
import fr.istic.aoc.metronome.engine.IEngine;
import fr.istic.aoc.metronome.engine.impl.ClockImpl;
import fr.istic.aoc.metronome.engine.impl.EngineImpl;
import fr.istic.aoc.metronome.view.IMetronomeView;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

/**
 * Metronome App.
 * Main Class of the application.
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 */
public class MainMetronomeApp extends Application {

    public static void main(String[] args) {
        // launch JavaFX application
        Application.launch(args);
    }

    /* - - - - - - -  Modules of the Application  - - - - - - - - - */
    IMetronomeView viewCtrl;    // ViewController (JavaFX)
    IController controller;     // Controller (Controller of the MVC)
    IEngine engine;             // Engine (Model of the MVC)
    IClock clock;               // Clock Service (Scheduler)


    @Override
    public void start(Stage stage) throws Exception {

        /*  - JavaFX View Initialization -  */
        URL url = getClass().getResource("/metronome.fxml");
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(url);
        Parent root = loader.load(url.openStream());

        /*  - JavaFX View Configuration -  */
        stage.setTitle(" * Metronome ReMx * ");
        stage.setScene(new Scene(root, 670, 410));
        stage.setResizable(false);
        stage.show();

        stage.setOnCloseRequest(e -> Platform.exit());

        //*  Init The MVC  *//
        viewCtrl = loader.getController();  // View Controller (JavaFX)
        engine = new EngineImpl();          // Engine   (Model)
        clock = new ClockImpl();            // Clock (Scheduler Service)
        controller = new ControllerImpl();  // Controller (MVC)
        controller.setClock(clock);
        controller.setView(viewCtrl);
        controller.setEngine(engine);

    }


    /**
     *
     * @throws Exception
     */
    @Override
    public void stop() throws Exception {
        //TODO : Check if the STOP METHOD of CONTROLLER kills the Thread of the Clock or not
        controller.stop();
    }





}
