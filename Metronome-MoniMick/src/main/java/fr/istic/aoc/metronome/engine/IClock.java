package fr.istic.aoc.metronome.engine;


import fr.istic.aoc.metronome.command.impl.TickCommand;

/**
 * Defines the interface for the IClock.
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 */
public interface  IClock {


    /**
     * Activate the TickCommand toute les periodes "seconds"
     * @param bpm
     */
    void activatePeriodically(double bpm);

    /**
     * Desactivate the TickCommand
     */
    void desactivateTickCommand();

    /**
     * Create the TickCommand with the bmp value
     * @param tick
     * @param bpm
     */
    void setTickCommand(TickCommand tick, double bpm);
}
