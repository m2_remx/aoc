package fr.istic.aoc.metronome.engine.impl;

import fr.istic.aoc.metronome.command.ICommand;
import fr.istic.aoc.metronome.engine.IEngine;

/**
 * Defines the interface for the IClock.
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 * @see fr.istic.aoc.metronome.engine.IEngine
 */
public class EngineImpl implements IEngine {

    //*  Constant : Value MAX of the Measure above which it is impossible to get  *//
    private static final int MEASUREMAX = 7;

    /*  --  Attributes  --  */
    private boolean isOn;
    private double bpm;
    private int measure;

    /*  --  Commands  --  */
    private ICommand updateEngineStateCommand;
    private ICommand setUpdateMeasureCommand;
    private ICommand engineUpdateBPMCommand;


    /*  --  Constructor  --  */
    public EngineImpl() {
        isOn = false;
        bpm = 120;
        measure = 4;
    }


    @Override
    public void start() {
        isOn = true;
        updateEngineStateCommand.execute();
    }

    @Override
    public void stop() {
        isOn = false;
        updateEngineStateCommand.execute();
    }

    @Override
    public boolean isOn() {
        return isOn;
    }


    /**/
    @Override
    public double getBPM() {
        return bpm;
    }

    @Override
    public void setBPM(double newBPM) {
        bpm = newBPM;
        engineUpdateBPMCommand.execute();
    }

    @Override
    public int getMeasure() {
        return measure;
    }

    @Override
    public void incMeasure() {
        measure++;
        setUpdateMeasureCommand.execute();
    }

    @Override
    public void decMeasure() {
        measure--;
        setUpdateMeasureCommand.execute();
    }

    @Override
    public void setUpdateEngineStateCommand(ICommand updateEngineState) {
        this.updateEngineStateCommand = updateEngineState;
    }

    @Override
    public void setUpdateMeasureCommand(ICommand updateMeasure) {
        this.setUpdateMeasureCommand = updateMeasure;
    }

    @Override
    public void setUpdateBPMCommand(ICommand updateBPMCommand) {
        this.engineUpdateBPMCommand = updateBPMCommand;
    }

    @Override
    public int getMeasureMax() {
        return MEASUREMAX;
    }

}
