package fr.istic.aoc.metronome.controller.impl;

import fr.istic.aoc.metronome.command.impl.TickCommand;
import fr.istic.aoc.metronome.controller.IController;
import fr.istic.aoc.metronome.engine.IClock;
import fr.istic.aoc.metronome.engine.IEngine;
import fr.istic.aoc.metronome.engine.impl.ClockImpl;
import fr.istic.aoc.metronome.view.IMetronomeView;
import javafx.fxml.Initializable;

/**
 * Defines our concrete Controller.
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 */
public class ControllerImpl implements IController {


    /* -    Attributes    - */
    private IEngine engine;
    private IClock clock;

    /*  -    ViewController    -  */
    private IMetronomeView viewController;

    /* Measure Counter */
    private int measureReference;   // Define the Measure Reference. (i.e.: Number of Steps that Defines One Measure)
    private int measureCounter;     // Is incremented to count each measure

    /* Measure Maximum */
    private int MEASUREMAX; // Initialized in the setEngine method


    /* Constructors */
    public ControllerImpl() {
        measureCounter = 0;
    }

    /**
     *
     */
    @Override
    public void turnOnLEDs() {
        if (measureCounter == 0) {
            viewController.turnOnLED2();
        }
        viewController.turnOnLED1();
        measureCounter = (measureCounter + 1) % measureReference;
    }

    /* ---  Getters & Setters  --- */

    @Override
    public void setView(IMetronomeView view) {
        this.viewController = view;


        //**  Commands creation (View - Controller)  **//
        //StartCommand
        viewController.setStartCmd( () -> turnEngineOn(true) );
        //StopCommand
        viewController.setStopCmd( () -> turnEngineOn(false));
        //IncCommand
        viewController.setIncCmd( () -> incMeasure() );
        //DecCommand
        viewController.setDecCmd( () -> decMeasure() );
        //BPMCommand
        viewController.setBPMCmd( () -> bpmHasChanged() );
    }

    @Override
    public void setEngine(IEngine engine) {
        this.engine = engine;
        /*   -   -   -   - SetCommand into the Engine -   -   -   -   */
        // UpdateEngineState Command)
        this.engine.setUpdateEngineStateCommand(() -> this.updateEngineState() );
        // UpdateMeasure Command)
        this.engine.setUpdateMeasureCommand(() -> this.updateMeasureReference());
        // UpdateBPM Command
        this.engine.setUpdateBPMCommand(() -> this.updateBPM());
        /*   -   -   -   -   -   -   -    -   -   -   -   -   -   -   */
        measureReference = engine.getMeasure();
        /*   -   -   -   -   -   -   -    -   -   -   -   -   -   -   */
        MEASUREMAX = engine.getMeasureMax();
    }

    @Override
    public void setClock(IClock clock) {
        this.clock = clock;
    }


    @Override
    public void turnEngineOn(boolean turnOn) {
        if( turnOn && !engine.isOn()) {
            engine.start();
        }
        else if( !turnOn && engine.isOn()) {
            engine.stop();

        }
    }

    @Override
    public void setTickCommand() {
        clock.setTickCommand(new TickCommand(this), engine.getBPM());
    }

    @Override
    public void stop() {
        clock.desactivateTickCommand();
    }


    @Override
    public void incMeasure() {
        if (engine.isOn() ){
            if (engine.getMeasure() < MEASUREMAX) {
                engine.incMeasure();
            }
        }
    }

    @Override
    public void decMeasure() {
        if (engine.isOn() ){
            if (engine.getMeasure() > 2) {
                engine.decMeasure();
            }
        }
    }

    @Override
    public void updateEngineState() {
        if(engine.isOn()) {
            setTickCommand();
            viewController.printBPM( engine.getBPM() );
            viewController.printMeasure( engine.getMeasure() );
        }
        else {
            stop();
        }
    }

    @Override
    public void updateMeasureReference() {
        measureReference = engine.getMeasure();
        viewController.printMeasure(measureReference);
    }

    @Override
    public void updateBPM() {
        clock.desactivateTickCommand();
        int currentBPM = (int) engine.getBPM();
        this.viewController.printBPM(currentBPM);
        clock.setTickCommand(new TickCommand(this), currentBPM);
    }



    /**
     * This Method is called by a BPMCommand (Controller is a receiver in the Command Pattern)
     *      when
     */
    @Override
    public void bpmHasChanged() {
        double newBPM = viewController.getBPMValue();
        engine.setBPM(newBPM);
    }

    //--- For the test --//
    public IClock getClock() {
        return clock;
    }

    public IEngine getEngine() {
        return engine;
    }
}
