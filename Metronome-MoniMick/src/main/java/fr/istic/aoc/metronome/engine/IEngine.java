package fr.istic.aoc.metronome.engine;

import fr.istic.aoc.metronome.command.ICommand;

/**
 * Defines the interface for the IEngine of our Metronome.
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 */
public interface IEngine {

    /* Start */
    void start();
    void stop();
    boolean isOn();

    /* BPM */
    double getBPM();
    void setBPM(double newBPM);

    /* Measure */
    int getMeasure();
    void incMeasure();
    void decMeasure();

    /**
     *
     * @param updateEngineState
     */
    void setUpdateEngineStateCommand(ICommand updateEngineState);

    /**
     *
     * @param updateMeasure
     */
    void setUpdateMeasureCommand(ICommand updateMeasure);

    /**
     *
     * @param updateBPMCommand
     */
    void setUpdateBPMCommand(ICommand updateBPMCommand);

    /**
     * @return the MEASURE of the engine above which it is impossible to go.
     */
    int getMeasureMax();
}
