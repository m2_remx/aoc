package fr.istic.aoc.metronome.controller;

import fr.istic.aoc.metronome.engine.IClock;
import fr.istic.aoc.metronome.engine.IEngine;
import fr.istic.aoc.metronome.view.IMetronomeView;

/**
 * Defines the interface for the Controller of our MVP conception.
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 */
public interface IController {

    /**
     * Set the View
     * @param view
     */
    void setView(IMetronomeView view);


    /**
     * Set the Engine
     * @param engine
     */
    void setEngine(IEngine engine);


    /**
     * Set the Clock
     * @param clock
     */
    void setClock(IClock clock);

    /**
     * Give a TickCommand to the Clock.
     * A TickCommand is a command called each time the Clock (IClock) ticks.
     */
    void setTickCommand();


    /**
     * Turns the LEDS of the View On.
     */
    void turnOnLEDs();


    /**
     * Method called when BPM has graphically changed (User Event).
     * This Method is called by a BPMCommand invoked by the View (the IController is a Receiver in the Command Pattern).
     * Change the BPM in the Engine.
     */
    void bpmHasChanged();


    /**
     * Turns ON or OFF the Engine.
     * @param turnOn : True to Activate, False to Desactivate the Engine.
     */
    void turnEngineOn(boolean turnOn);


    /**
     * Desactive the Clock and its TickCommand
     */
    void stop();


    /**
     * Increments the number of steps that defines the Measure.
     */
    void incMeasure();


    /**
     * Decrements the number of steps that defines the Measure.
     */
    void decMeasure();


    /**
     * Tells The view that the Engine has a new BPM
     */
    void updateEngineState();


    /**
     * Update the value of the Measure Reference of the Controller by getting it from the Engine
     * IController.measureReference = IEngine.getMeasure();
     */
    void updateMeasureReference();


    /**
     * Update the value of the BMP of the Controller
     */
    void updateBPM();


}
