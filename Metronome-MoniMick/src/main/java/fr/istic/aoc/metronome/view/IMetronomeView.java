package fr.istic.aoc.metronome.view;

import fr.istic.aoc.metronome.command.ICommand;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;


/**
 * Created by leiko on 23/10/15.
 * Modified by Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 */
public interface IMetronomeView extends Initializable {

    //*   - - -      Listeners      - - -   *//

    /**
     * Listener of Start Event
     * @param event (Start Event)
     */
    void startEvent(ActionEvent event);

    /**
     * Listener of Stop Event
     * @param event (Stop Event)
     */
    void stopEvent(ActionEvent event);

    /**
     * Listener of Increment Measure Event
     * @param event (Increment Measure Event)
     */
    void incEvent(ActionEvent event);

    /**
     * Listener of Decrement Measure Event
     * @param event (Decrement Measure Event)
     */
    void decEvent(ActionEvent event);

    /**
     * Add a Listener of Event on the Slider
     */
    void initSliderBPMEvent();



    //*   - - -      Commands      - - -   *//

    /**
     * Activate the LED 1 ( Mark each Beat)
     */
    void turnOnLED1() ;

    /**
     * Activate the LED 2 ( Mark each Measure)
     */
    void turnOnLED2() ;

    /**
     * BPM Getter
     * @return the current BPM Value of the View
     */
    double getBPMValue() ;

    /**
     * Measure Printer
     * Print the current Measure Value from the Engine to the View
     */
    void printMeasure(int measure) ;

    /**
     *
     * @param bpm
     */
    void printBPM(double bpm);

    /**
     *
     * @param startCmd
     */
    void setStartCmd(ICommand startCmd);

    /**
     *
     * @param stopCmd
     */
    void setStopCmd(ICommand stopCmd);

    /**
     *
     * @param incCmd
     */
    void setIncCmd(ICommand incCmd);

    /**
     *
     * @param decCmd
     */
    void setDecCmd(ICommand decCmd) ;

    /**
     *
     * @param bpmCommand
     */
    void setBPMCmd(ICommand bpmCommand);
}