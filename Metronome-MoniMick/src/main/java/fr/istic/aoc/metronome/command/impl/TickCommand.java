package fr.istic.aoc.metronome.command.impl;

import fr.istic.aoc.metronome.command.ICommand;
import fr.istic.aoc.metronome.controller.IController;

/**
 * This Command is called each time the CLOCK (IClock) ticks
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 * @see fr.istic.aoc.metronome.command.ICommand
 */
public class TickCommand implements ICommand{

    /* Receiver */
    private IController controller;

    /* Constructor */
    public TickCommand(IController controller) {
        this.controller = controller;
    }


    /**
     * Asks the controller to turns the LEDS of the View on
     */
    @Override
    public void execute() {
        controller.turnOnLEDs();
    }
}
