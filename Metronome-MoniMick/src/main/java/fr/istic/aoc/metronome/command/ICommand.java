package fr.istic.aoc.metronome.command;

/**
 * Defines the interface for the different Commands of our Metronome.
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 */
@FunctionalInterface
public interface ICommand {
    void execute();
}
