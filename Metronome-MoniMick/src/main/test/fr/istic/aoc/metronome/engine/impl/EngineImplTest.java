package fr.istic.aoc.metronome.engine.impl;

import fr.istic.aoc.metronome.engine.IEngine;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test of EngineImpl class
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 */
public class EngineImplTest {

    IEngine engine = new EngineImpl();

    @Before
    public void initEngine(){
        engine = new EngineImpl();
        engine.setUpdateEngineStateCommand(() -> System.out.println("setUpdateEngineStateCommand"));
        engine.setUpdateMeasureCommand(() -> System.out.println("setUpdateMeasureCommand"));
        engine.setUpdateBPMCommand(() -> System.out.println("setUpdateBPMCommand"));

    }

    @Test
    public void testDecMeasure() throws Exception {
        assertEquals(4, engine.getMeasure());
        engine.decMeasure();
        engine.decMeasure();
        engine.decMeasure();
        assertEquals(1, engine.getMeasure());
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testStart() throws Exception {
        assertFalse(engine.isOn());
        engine.start();
        assertTrue(engine.isOn());
    }

    @Test
    public void testStop() throws Exception {
        engine.start();
        assertTrue(engine.isOn());
        engine.stop();
        assertFalse(engine.isOn());
    }

    @Test
    public void testSetBPM() throws Exception {
        assertEquals(120, engine.getBPM(),0);
        engine.setBPM(300);
        assertEquals(300, engine.getBPM(),0);
    }

    @Test
    public void testIncMeasure() throws Exception {
        assertEquals(4, engine.getMeasure());
        engine.incMeasure();
        engine.incMeasure();
        assertEquals(6, engine.getMeasure());
    }


}