package fr.istic.aoc.metronome.controller.impl;

import fr.istic.aoc.metronome.controller.IController;
import fr.istic.aoc.metronome.engine.IEngine;
import fr.istic.aoc.metronome.engine.impl.ClockImpl;
import fr.istic.aoc.metronome.engine.impl.EngineImpl;
import fr.istic.aoc.metronome.view.IMetronomeView;
import fr.istic.aoc.metronome.view.impl.MetronomeViewImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

/**
 * Unit test of ControllerImpl class
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 */
@RunWith(MockitoJUnitRunner.class)
public class ControllerImplTest {

    IController controller = new ControllerImpl();

    @Before
    public void initEngine(){
        IEngine engine = new EngineImpl();
        controller = new ControllerImpl();
        controller.setEngine(engine);

        IMetronomeView view = Mockito.mock(MetronomeViewImpl.class);
        controller.setView(view);
    }

    @Test
    public void testSetTickCommand() {
        assertNull(((ClockImpl)((ControllerImpl)controller).getClock()).getTickCommand());
        controller.setTickCommand();
        assertNotNull(((ClockImpl)((ControllerImpl)controller).getClock()).getTickCommand());
    }

    @Test
    public void testTurnEngineOn(){
        assertNotNull(((ControllerImpl)controller).getEngine());
        assertFalse(((ControllerImpl)controller).getEngine().isOn());
        controller.turnEngineOn(true);
        assertTrue(((ControllerImpl)controller).getEngine().isOn());
    }

    @Test
    public void testIncMeasure() throws Exception {
        assertFalse(((ControllerImpl)controller).getEngine().isOn());
        controller.turnEngineOn(true);
        assertTrue(((ControllerImpl)controller).getEngine().isOn());
        assertTrue(((ControllerImpl)controller).getEngine().getMeasure()==4);
        controller.incMeasure();
        controller.incMeasure();
        assertTrue(((ControllerImpl)controller).getEngine().getMeasure()==6);
    }

    @Test
    public void testDecMeasure() throws Exception {
        assertFalse(((ControllerImpl)controller).getEngine().isOn());
        controller.turnEngineOn(true);
        assertTrue(((ControllerImpl)controller).getEngine().isOn());
        assertTrue(((ControllerImpl)controller).getEngine().getMeasure()==4);
        controller.decMeasure();
        assertTrue(((ControllerImpl)controller).getEngine().getMeasure()==3);
    }

    @Test
    public void testUpdateEngineState() throws Exception {
        assertFalse(((ControllerImpl)controller).getEngine().isOn());
        controller.updateEngineState();
        assertFalse(((ControllerImpl)controller).getEngine().isOn());

    }

}